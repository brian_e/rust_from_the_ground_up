use std::collections::HashMap;

const DEFAULT_GREETINGS: [(&'static str, &'static str); 2] = [("english", "hello world"),
                                                              ("italian", "ciao mondo")];

pub struct Greeter {
    greetings: HashMap<String, String>,
}

impl Greeter {
    pub fn new() -> Greeter {
        let mut greetings: HashMap<String, String> = HashMap::new();
        for tuple in &DEFAULT_GREETINGS {
            let (ref language, ref greeting) = *tuple;
            greetings.insert(String::from(*language), String::from(*greeting));
        }
        return Greeter { greetings: greetings };
    }

    pub fn add_greeting(&mut self, language: String, greeting: String) {
        self.greetings.insert(language, greeting);
    }

    pub fn get_greeting(&self, language: String) -> Result<String, String> {
        return match self.greetings.get(&language as &str) {
            Some(greeting) => Ok(greeting.clone()),
            None => Err(format!("language not found: {}", language)),
        };
    }
}
