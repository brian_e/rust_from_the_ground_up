extern crate groundup;

use groundup::Greeter;

fn main() {
    let greeter1 = &mut Greeter::new();
    let greeter2 = &Greeter::new();

    for _ in 0..2 {

        for language in &["english", "italian", "spanish", "french"] {
            let mut i = 1;
            for greeter in &[greeter1, greeter2] {
                match greeter.get_greeting(String::from(*language)) {
                    Ok(greeting) => println!("greeter #{}: {}", i, greeting),
                    Err(message) => println!("WARNING: greeter #{}: {}", i, message),
                };
                i += 1;
            }
        }

        for tuple in &[("spanish", "hola mundo"), ("french", "bonjour le monde")] {
            let (ref language, ref greeting) = *tuple;
            greeter1.add_greeting(String::from(*language), String::from(*greeting));
        }

    }
}
