#! /bin/bash

for dir in groundup_*; do
	pushd ${dir}
	cargo fmt -- --write-mode=overwrite
	popd
done
