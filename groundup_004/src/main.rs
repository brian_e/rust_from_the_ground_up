extern crate groundup;

use groundup::greeting;

fn main() {
    for language in &["spanish", "french"] {
        println!("{}", greeting(String::from(*language)));
    }
}
