use std::collections::HashMap;

pub fn greeting(language: String) -> String {
    let mut greetings: HashMap<&str, &str> = HashMap::new();
    for tuple in &[("spanish", "hola mundo"), ("french", "bonjour le monde")] {
        let (ref language, ref greeting) = *tuple;
        greetings.insert(language, greeting);
    }
    if let Some(greeting) = greetings.get(&language as &str) {
        return String::from(*greeting);
    }
    panic!("language not found: {}", language);
}
