#! /bin/bash

for dir in groundup_*; do
	pushd ${dir}
	cargo run
	popd
done
