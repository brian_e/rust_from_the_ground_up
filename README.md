# Rust from the ground up

I work on a Mac and use Sublime Text 3.

Install Rust using rustup.rs. The benefit of installing this way is that you can switch between the stable and nightly versions of the compiler. You will need the nightly version to use the benchmarking feature.

    curl https://sh.rustup.rs -sSf | sh
    
    # 2) Customize installation
    2

    # Default toolchain? (stable/beta/nightly)
    stable

    # Modify PATH variable? (y/n)
    n

    # 1) Proceed with installation (default)
    1


Add Cargo's bin directory in your PATH environment variable in the current shell. CARGO_HOME will be used by the Sublime Text plugin.

    $ export CARGO_HOME="${HOME}/.cargo"
    $ export PATH="${CARGO_HOME}/bin:${PATH}"


Add Cargo's bin directory in your PATH environment variable in future shells.

    $ echo '' >> ~/.bash_profile
    $ echo '# Rust lang stuff' >> ~/.bash_profile
    $ echo 'export CARGO_HOME="${HOME}/.cargo"' >> ~/.bash_profile
    $ echo 'export PATH="${CARGO_HOME}/bin:${PATH}"' >> ~/.bash_profile


Create a new cargo project and open the editor.

    $ cargo new groundup
    $ echo 'Cargo.lock' >> .gitignore
    $ echo 'target' >> .gitignore
    $ cd groundup
    $ touch src/main.rs
    $ RUST_BACKTRACE=1 subl .


Another goodie is rustfmt, a code formatter.

    $ cargo install rustfmt
    $ cargo fmt


I like the RustAutoComplete package from Sublime Text. Use Package control to install it. It supports
Go to definition (default key binding is F2) which is a nice way to peruse the standard library. It depends on racer being installed.

    $ cargo install racer
    $ cd ~/
    $ git clone git@github.com:rust-lang/rust.git
    $ export RUST_SRC_PATH="${HOME}/rust/src"
    $ echo 'export RUST_SRC_PATH="${HOME}/rust/src"' >>  ~/.bash_profile

    $ cd ${RUST_SRC_PATH}

    $ rustup show
    rustc 1.9.0 (e4e8b6668 2016-05-18)

    $ git checkout -q 1.9.0


## groundup v0.0.1 - Hello World

Goal: print 'hello world' the screen.

Opinions:

* When you are first learning, prefer String over string slices (&str). String will work in a broader range of cases.
* Explicitly state type in all variable declarations. Use the `let () = x;` to print out an error message with the type.
* Assign to variables for intermediate steps. Especially when you get stuck. Very simple looking Rust code can be doing many type conversions behind the scenes.

Solution: [source code](groundup_001/src)

CMD-SHIFT-B in sublime and choose 'cargo run'. You can do 'CMD-B' from now on and it will remember that you chose 'cargo run'.

## groundup v0.0.2 - importing from lib.rs

[source code](groundup_002/src)

Add a `src/lib.rs` file and modify `main.rs` to use it.

## groundup v0.0.3 - vectors

[source code](groundup_003/src)

## groundup v0.0.4 - hash maps

[source code](groundup_004/src)

## groundup v0.0.5 - classes

[source code](groundup_005/src)
