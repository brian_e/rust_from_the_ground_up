extern crate groundup;

fn main() {

    // or with all the magic of Rust...
    for greeting in groundup::get_greetings_macro() {
        println!("{}", greeting);
    }

    // or with all the magic of Rust...
    for greeting in groundup::get_greetings() {
        println!("{}", greeting);
    }

    // without the magic so you can see what is going on...
    let greetings: Vec<String> = groundup::get_greetings_explictly();
    let greetings_iter: std::vec::IntoIter<String> = greetings.into_iter();
    for greeting_ref in greetings_iter {
        // greeting_ref type is &String
        // let () = greeting_ref;
        let greeting: String = greeting_ref.clone();
        println!("{}", greeting);
    }

}
