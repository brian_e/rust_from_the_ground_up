pub fn get_greetings_macro() -> Vec<String> {
    // in this simple example this is a reasonable way to do it...
    return vec![String::from("hello world"), String::from("hola mundo")];
}

pub fn get_greetings() -> Vec<String> {
    // this is probably more real-world where if you have a long list
    // and wanted to convert in a loop
    let mut greetings: Vec<String> = Vec::new();
    for greeting in &["hello world", "hola mundo"] {
        // greeting is a reference to a string slice (&&str)
        // let () = greeting;
        greetings.push(String::from(*greeting));
    }
    return greetings;
}

pub fn get_greetings_explictly() -> Vec<String> {
    let mut greetings: Vec<String> = Vec::new();
    let greetings_array: [&str; 2] = ["hello world", "hola mundo"];
    let greetings_slice: &[&str] = &greetings_array;
    let greetings_iter: std::slice::Iter<&str> = greetings_slice.into_iter();
    for greeting_slice_ref in greetings_iter {
        // the type of greeting_slice_ref is &&str
        // let () = greeting_ref_ref;
        let greeting_slice: &str = *greeting_slice_ref;
        greetings.push(String::from(greeting_slice));
    }
    return greetings;
}
