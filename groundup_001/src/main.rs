fn main() {

    println!("hello world");

    let str_slice: &str = "hello world";
    println!("{}", str_slice);

    // three ways to construct a String from a string slice (&str)
    let a: String = str_slice.to_string();
    println!("{}", a);

    let b: String = String::from(str_slice);
    println!("{}", b);

    let c: String = str_slice.to_owned();
    println!("{}", c);

    // debug printout (notice the quotes in the output)
    println!("{:?}", String::from("hello world"));
}
